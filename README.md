# wavechild

This is a basic child theme for Wave-Parent-Theme from OXID eShop.

## Installation  
composer config repo.swinde/kfe-theme git https://bitbucket.org/swinde/kfe-theme.git

composer require --no-scripts --update-no-dev --no-interaction --optimize-autoloader swinde/kfe-theme